<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 8/28/15
 * Time: 3:34 PM
 * @var $this MGT_CacheFull_Model_Observer
 */

class MGT_CacheFull_Model_Observer {

    public function controller_action_layout_generate_blocks_after(Varien_Event_Observer $observer){
        $layout = $observer->getEvent()->getLayout();
        $action = $observer->getEvent()->getAction();
        $cacheRoute = array("catalog","cms","technical");
        if($layout->getArea() == "frontend" && in_array($action->getRequest()->getRouteName(),$cacheRoute)){
            $cacheTime = 99999999;
            $currentUrl = Mage::helper('core/url')->getCurrentUrl();

            $content = $layout->getBlock('content');
            $content->setCacheLifetime($cacheTime);
            $content->setCacheKey("content:" . $currentUrl);

            $left = $layout->getBlock('left');
            if($left){
                $left->setCacheLifetime($cacheTime);
                $left->setCacheKey("left:" .$currentUrl);
            }

            $head = $layout->getBlock('head');
            $head->setCacheLifetime($cacheTime);
            $head->setCacheKey("head:" .$currentUrl);

            $header_register = $layout->getBlock('header_register');
            if($header_register){
                $header_register->setCacheLifetime($cacheTime);
                $header_register->setCacheKey("header_register:" .$currentUrl);
            }

            $header_login = $layout->getBlock('header_login');
            if($header_login){
                $header_login->setCacheLifetime($cacheTime);
                $header_login->setCacheKey("header_login:" .$currentUrl);
            }

            $right = $layout->getBlock('right');
            if($right){
                $right->setCacheLifetime($cacheTime);
                $right->setCacheKey("right:" .$currentUrl);
            }

        }
    }
}