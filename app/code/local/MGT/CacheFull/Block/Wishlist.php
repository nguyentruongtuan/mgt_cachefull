<?php
/**
 * Wishlist Block
 *
 * @category    Wishlist
 * @package     MGT_CacheFull
 * @author      MGT Magento Team <tuantruong@millergoldtech.com>
 *
 * @var MGT_CacheFull_Block_Wishlist $this
 */

class MGT_CacheFull_Block_Wishlist extends Mage_Core_Block_Template{

    public function getWishlistData(){
        $session = Mage::getSingleton('customer/session');
        $customer_id = $session->getId();
        if(!$customer_id && !empty($_SESSION['random_customer_id']))
            $customer_id = $_SESSION['random_customer_id'];
        if($customer_id){
            $wishlist = Mage::getModel('wishlist/item')->getCollection();
            $wishlist->getSelect()
                ->join(array('t2' => 'wishlist'),
                    'main_table.wishlist_id = t2.wishlist_id',
                    array('wishlist_id','customer_id'))
                ->where('t2.customer_id='.$customer_id);
            $items = $wishlist->load()->getItems();
            return $items;
        }

        return array();
    }

}